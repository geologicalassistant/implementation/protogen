package com.geoassistant.scenariogen;

import org.semanticweb.owlapi.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.Queue;

public class OntologyPermuter extends Ontology {

    private static final Logger LOGGER = LoggerFactory.getLogger(OntologyPermuter.class);

    // reserved TBox class names
    private String unknownClassName = "Unknown";

    private static final String NON_PERMUTABLE = "NonPermutable";

    private static final String MAUDE_FILE_SUFFIX = ".maude";

    // path for maude outputs
    private String outputPath;

    private boolean useSemanticIDs = false;

    private int combinationCounter = 0;
    private Set<Set<OWLClassAssertionAxiom>> combinations = new HashSet<>();
    private Set<OWLClassAssertionAxiom> combination = new HashSet<>();

    public OntologyPermuter(boolean debugMode) {
        super(debugMode);
    }

    // used from test only
    public OntologyPermuter(String reservedClassName) {
        super(false);
        unknownClassName = reservedClassName;
    }

    /**
     * Initializes the permuter by loading the ontology with the given filename and by setting the outputPath for the generated files.<p>
     *     Note that the outputPath is the relative path and base-filename to be used. A counter and the .maude suffix will be added by the permuter.
     */
    public void initialize(String filename, String outputPath) throws OWLOntologyCreationException {
        loadOntology(filename);

        if (outputPath != null && !outputPath.trim().isEmpty()) {
            setOutputPath(outputPath);
        }
    }

    private void setOutputPath(String path) {
        this.outputPath = path;
    }

    public void setUseSemanticIDs(boolean useSemanticIDs) {
        this.useSemanticIDs = useSemanticIDs;
    }

    /**
     * Initial call to start the permutation process. This process generates new ontologies based on the
     * rules in the ontology. More documentation should be available somewhere.
     *
     * The ontology needs to follow as certain "style(?)" to be able to use this permutation method.
     * @throws Exception
     */
    public void permute() {
        LOGGER.debug("initial ontology");
        OntologyUtils.printClassAssertions(getOntology());
        LOGGER.debug("***");
        OWLClass thing = getFactory().getOWLThing();

        Set<OWLClassAssertionAxiom> unknownSet = getClassAssertionAxioms(unknownClassName);
        OWLClassAssertionAxiom[] unknownArr = unknownSet.toArray(new OWLClassAssertionAxiom[0]);
        List<OWLClassAssertionAxiom> permutables = new ArrayList<>(Arrays.asList(unknownArr));

        if (!permutables.isEmpty()) {
            permute(permutables, thing);
        }
    }

    /**
     * Intermediary permuting call. Not really necessary?
     * Needed to add children of owl:Thing! Otherwise must check for owl:Thing in recursive call (not allowed to
     * add class assertion axiom with owl:Thing to ontology during run).
     *
     * @param permutables
     * @throws Exception
     */
    private void permute(List<OWLClassAssertionAxiom> permutables, OWLClass root) {
        OWLClassAssertionAxiom unknownCAA = permutables.get(0);
        permutables = Utils.copyWithoutFirstElement(permutables);
        OWLIndividual unknown = unknownCAA.getIndividual();

        // the unknown class assertion axiom no longer needed
        getManager().removeAxiom(getOntology(), unknownCAA);

        // need to handle owl:Thing (root) here, because it cannot be added to the ABox without getting an
        // exception. the alternative is to check in every recursive call below if the node is owl:Thing

        Queue<OWLClass> queue = new LinkedList<>();
        for (OWLClass c : getReasoner().getSubClasses(root, true).getFlattened()) {
            if (!c.getIRI().getShortForm().equals(unknownClassName) && !c.getIRI().getShortForm().equals(NON_PERMUTABLE)) {
                queue.add(c);
            }
        }

        try {
            bfs(permutables, queue, unknown);
        }
        catch(IOException e) {
            LOGGER.error("error while writing ontology to file: {}", e.getMessage(), e);
        }
    }

    /**
     * Recursive breadth-first traversal of the class hierarchy for an individual.
     *
     * The traversal code is inspired by https://www.techiedelight.com/breadth-first-search/
     * @param queue
     * @throws Exception
     */
    private void bfs(List<OWLClassAssertionAxiom> permutables, Queue<OWLClass> queue, OWLIndividual unknown) throws IOException {
        // when the breadth-first traversal is finished - also checked for consistency
        if (queue.isEmpty() && permutables.isEmpty()) {
            // end of one combination when there are more nodes in the queue
            if (!Utils.subsetOf(combination, combinations)) {
                LOGGER.info("combinationCounter={} ", ++combinationCounter);
                OntologyUtils.prettyprint(combination);

                if (outputPath != null) {
                    MaudeSerializer.writeFile(getOntology(), getReasoner(), outputPath + combinationCounter + MAUDE_FILE_SUFFIX, this.unknownClassName, useSemanticIDs);
                } else {
                    // If no output path is given, print to Standard Out
                    LOGGER.info(MaudeSerializer.serializeAbox(getOntology(), getReasoner(), this.unknownClassName));
                }

                Set<OWLClassAssertionAxiom> result = new HashSet<>(combination);
                combinations.add(result);
            }
            return;
        } else if (queue.isEmpty()) {
            permute(permutables, getFactory().getOWLThing());
            return;
        }

        // pop front node from queue and print it
        OWLClass currentClass = queue.poll();
        Queue<OWLClass> queueCopy = new LinkedList<>(queue);

        // do something with current node
        // want to do it first with the current node so that the added combinations are maximal
        OWLClassAssertionAxiom ax = getFactory().getOWLClassAssertionAxiom(currentClass, unknown);
        boolean containedAxiom = getOntology().containsAxiom(ax);

        if (!containedAxiom) {
            // update ABox
            getManager().addAxiom(getOntology(), ax);
            getReasoner().flush();
        }

        // continue traversing the hierarchy with the current node
        // (cannot be inside of the previous if in case the class was already assigned)
        if (getReasoner().isConsistent()) {
            // add current node to current combination
            combination.add(ax);

            // add children of current node
            for (OWLClass c : getReasoner().getSubClasses(currentClass, true).getFlattened()) {
                // skip adding owl:Nothing - don't care about leaf nodes
                if (!c.isOWLNothing()) {
                    queue.add(c);
                }
            }

            bfs(permutables, queue, unknown);

            // remove the current node from the current combination (for traversal without the node)
            combination.remove(ax);
        }

        if (!containedAxiom) {
            // update ABox
            getManager().removeAxiom(getOntology(), ax);
            getReasoner().flush();
        }

        // continue traversing the hierarchy without the current node
        bfs(permutables, queueCopy, unknown);
    }
}
