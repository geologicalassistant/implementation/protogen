package com.geoassistant.scenariogen;

import org.semanticweb.HermiT.Reasoner;
import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.*;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.semanticweb.owlapi.vocab.PrefixOWLOntologyFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;

/**
 * This abstract class is a way to represent an ontology in the OWLAPI.
 * Each ontology will have its own Ontology Manager.
 *
 * Its purpose is currently to be separated into a TemplateOntology and
 * a WorkerOntology, and these two will communicate with each other.
 * They will be duplicates of each other except for the individuals.
 *
 * Useful links:
 * - OWLAPI documentation: https://github.com/owlcs/owlapi/wiki
 * - Javadoc: http://owlcs.github.io/owlapi/apidocs_4/index.html
 */
public abstract class Ontology {

    private static final Logger LOGGER = LoggerFactory.getLogger(Ontology.class);

    private final boolean debugMode;

    // the OWL ontology manager
    private OWLOntologyManager manager;

    // OWLAPI variables
    private OWLOntology ontology;
    private OWLReasoner reasoner;
    private OWLDataFactory factory;
    private PrefixOWLOntologyFormat pm;

    protected Ontology(boolean debugMode) {
        this.debugMode = debugMode;
        manager = OWLManager.createOWLOntologyManager();
    }

    /**
     * Load an ontology from a file from same folder as the java source file. Also instantiates a Reasoner object.
     * @param filename name of ontology file
     * @throws Exception
     */
    protected void loadOntology(String filename) throws OWLOntologyCreationException {
        // only one ontology per ontology?
        if (ontology != null) {
            throw new IllegalStateException("Already loaded an ontology.");
        }

        // ontology file related variables
        File ontologyFile = new File(filename);

        ontology = manager.loadOntologyFromOntologyDocument(ontologyFile);
        reasoner = new Reasoner.ReasonerFactory().createReasoner(ontology);

        // check consistency
        if (!reasoner.isConsistent()) {
            throw new IllegalStateException("Loaded ontology is not consistent.");
        }

        factory = manager.getOWLDataFactory();
        pm = (PrefixOWLOntologyFormat) manager.getOntologyFormat(ontology);

        if (debugMode) {
            LOGGER.debug("Loaded template ontology: {}", ontology.getOntologyID());
        }
    }

    /**
     * Get all class assertion axioms concerning className (all individuals instantiated to be this class).
     * (Used to generate the list of permutables)
     *
     * @param className name of class to get assertion axioms of
     * @return A set with all class assertion axioms that is of class className
     */
    public Set<OWLClassAssertionAxiom> getClassAssertionAxioms(String className) {
        OWLClassExpression oce = factory.getOWLClass(className, pm);
        return ontology.getClassAssertionAxioms(oce);
    }

    /**
     * Checks if a class is a leaf class. Leaf class as in it has no sub classes.
     *
     * Returns true iff the direct sub class of the class expression is only bottom/OWL:Nothing
     * @param ce class expression to check
     * @return true if leaf class, otherwise false
     */
    protected boolean isLeafClass(OWLClassExpression ce) {
        return reasoner.getSubClasses(ce, true).isBottomSingleton();
    }

    public Set<OWLClassAssertionAxiom> getLeafClasses(Set<OWLClassAssertionAxiom> set) {
        Set<OWLClassAssertionAxiom> result = new HashSet<>();

        for (OWLClassAssertionAxiom ax : set) {
            if (isLeafClass(ax.getClassExpression())) {
                result.add(ax);
            }
        }

        return result;
    }

    protected OWLOntologyManager getManager() {
        return manager;
    }

    public OWLOntology getOntology() {
        return ontology;
    }

    protected OWLReasoner getReasoner() {
        return reasoner;
    }

    protected OWLDataFactory getFactory() {
        return factory;
    }
}
