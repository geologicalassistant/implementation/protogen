package com.geoassistant.scenariogen;

import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLObjectPropertyAssertionAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.reasoner.OWLReasoner;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Set;

public final class MaudeSerializer {

    private static final Logger LOGGER = LoggerFactory.getLogger(MaudeSerializer.class);

    private static final String PREFIX = "mod OWL-OUTPUT is\n" +
            "  protecting CONFIGURATION .\n" +
            "  protecting OWL-CONVERTER .\n" +
            "  protecting SPATIAL .\n";
    private static final String POSTFIX = "\nendm";

    private MaudeSerializer() {
    }

    public static String serializeAbox(OWLOntology ontology, OWLReasoner reasoner, String unknownClassName) {
        Set<OWLNamedIndividual> individuals = ontology.getIndividualsInSignature();
        StringBuilder result = new StringBuilder(PREFIX);
        result.append("  op initDef : -> Configuration .\n");
        result.append("  eq initDef = ");

        for (OWLNamedIndividual i : individuals) {
            String oid = i.getIRI().getShortForm();
            Set<OWLClassAssertionAxiom> instantiations = ontology.getClassAssertionAxioms(i);

            for (OWLClassAssertionAxiom cax : instantiations) {
                if (reasoner.getSubClasses(cax.getClassExpression(), true).isBottomSingleton()
                    && !cax.getClassExpression().asOWLClass().getIRI().getShortForm().equals(unknownClassName)) {
                    String sort = cax.getClassExpression().asOWLClass().getIRI().getShortForm();
                    result.append("type(").append(sort).append(", ").append(oid).append(") ");
                }
            }

            // object properties
            Set<OWLObjectPropertyAssertionAxiom> axioms = ontology.getObjectPropertyAssertionAxioms(i);
            for (OWLObjectPropertyAssertionAxiom ax : axioms) {
                String prop = ax.getProperty().asOWLObjectProperty().getIRI().getShortForm();
                String subject = ax.getSubject().asOWLNamedIndividual().getIRI().getShortForm();
                String object = ax.getObject().asOWLNamedIndividual().getIRI().getShortForm();

                result.append(prop).append('(').append(subject).append(", ").append(object).append(") ");
            }
        }

        result.append('.');
        result.append(POSTFIX);

        return result.toString();
    }

    public static void writeFile(OWLOntology ontology, OWLReasoner reasoner, String filename, String unknownClassName, boolean useSemanticIDs) throws IOException {
        String output = serializeAbox(ontology, reasoner, unknownClassName);
        if(!useSemanticIDs) {
            String ontologyID = ontology.getOntologyID().getOntologyIRI().getShortForm();
            output = output.replace(ontologyID + '#', "");
        }

        String workingDir = System.getProperty("user.dir");
        File file = new File((filename.contains("/") || filename.contains("\\")) ? (workingDir + filename) : filename);

        try(BufferedWriter writer = new BufferedWriter(new FileWriter(file))) {
            writer.write(output);
        }
    }
}
