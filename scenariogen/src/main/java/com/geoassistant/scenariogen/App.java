package com.geoassistant.scenariogen;

import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

/**
 *
 */
public class App 
{
    private static final Logger LOGGER = LoggerFactory.getLogger(App.class);

    private static final String DEBUG_PARAMETER = "-debug";
    private static final String SEMANTIC_ID_PARAMETER = "-id";

    public static void main(String[] args) {
        LOGGER.info("Parameters: {}", args);

        List<String> argsLst = new LinkedList<>(Arrays.asList(args));

        // Handle flags by removing them from list:
        boolean debugMode = argsLst.remove(DEBUG_PARAMETER);

        // Print usage if not input files are given
        if (argsLst.size() == 0) {
            printUsage();
            return;
        }

        OntologyPermuter op = new OntologyPermuter(debugMode);

        try {
            op.initialize(argsLst.get(0), argsLst.get(1));

            if(isSemanticIdsParameterSet(args)) {
                op.setUseSemanticIDs(true);
            }

            op.permute();
        }
        catch(OWLOntologyCreationException e) {
            LOGGER.error("error loading ontology: {}", e.getMessage(), e);
        }
    }

    private static boolean isSemanticIdsParameterSet(String[] args) {
        return (args.length == 3 && SEMANTIC_ID_PARAMETER.equals(args[2]))
                || args.length == 4 && SEMANTIC_ID_PARAMETER.equals(args[3]);
    }

    private static void printUsage() {
        System.out.println("Usage: ProtoScenarioGenerator [-debug] <input-file> <output-folder> [-id]\n"
                           + "where\n"
                           + "\t<input-file> is the input OWL-file to permute\n"
                           + "\t<output-folder> is the output folder to write permutation Aboxes to\n"
                           + "\t[-id] switch to use \"semantic-style\" object IDs instead of numbers only");
    }
}
