package com.geoassistant.scenariogen;

import org.semanticweb.owlapi.model.OWLAnonymousIndividual;
import org.semanticweb.owlapi.model.OWLAxiom;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLClassExpression;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLIndividualAxiom;
import org.semanticweb.owlapi.model.OWLOntology;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Set;

public final class OntologyUtils {

    private static final Logger LOGGER = LoggerFactory.getLogger(OntologyUtils.class);

    private OntologyUtils() {

    }

    private static <T> void printSet(Set<T> set) {
        for (T o : set) {
            LOGGER.debug(o.toString());
        }

        LOGGER.debug("");
    }

    public static void printClassAssertions(OWLOntology ontology) {
        Set<OWLAxiom> axioms = ontology.getAxioms();
        LOGGER.debug(axioms.toString());
        for (OWLAxiom axiom : axioms) {
            if (axiom instanceof OWLClassAssertionAxiom) {
                LOGGER.debug(axiom.toString());
            }
        }

        LOGGER.debug("");
    }

    public static void printAnonymous(OWLOntology ontology) {
        Set<OWLAnonymousIndividual> anons = ontology.getAnonymousIndividuals();
        LOGGER.debug(anons.toString());

        for (OWLAnonymousIndividual anon : anons) {
            LOGGER.debug(anon.toString());
            Set<OWLIndividualAxiom> axioms = ontology.getAxioms(anon);
            LOGGER.debug(axioms.toString());
            LOGGER.debug("");
        }
    }

    public static void printClassAxioms(OWLOntology ontology, OWLClassExpression oce) {
        printSet(ontology.getClassAssertionAxioms(oce));

    }

    public static void printIndividualClassAxioms(OWLOntology ontology, OWLIndividual i) {
        printSet(ontology.getClassAssertionAxioms(i));
    }

    public static boolean classInSet(OWLOntology ontology, OWLClass c, Set<OWLClassExpression> others) {
        for (OWLClassExpression o : others) {
            if (c.equals(o)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Prettyprint a set of class assertions
     */
    public static void prettyprint(Set<OWLClassAssertionAxiom> set) {
        for (OWLClassAssertionAxiom ax : set) {
            String className = ax.getClassExpression().asOWLClass().getIRI().getShortForm();
            String individual = ax.getIndividual().asOWLNamedIndividual().getIRI().getShortForm();

            LOGGER.debug("{}({}) ", className, individual);
        }
        LOGGER.debug("");
    }

    /**
     * Print all leaf class assertions in the set.
     *
     * Did not work on forcetwoattributes.owl ontology. Somehow Porous and NonPorous were not printed.
     * They were not considered leaf classes?
     *
     * @param set
     * @param ont
     */
    public static void leafprint(Set<OWLClassAssertionAxiom> set, Ontology ont) {
        prettyprint(ont.getLeafClasses(set));
    }
}
