package com.geoassistant.scenariogen;

import org.semanticweb.owlapi.model.OWLClass; 
import org.semanticweb.owlapi.model.OWLClassAssertionAxiom;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLNamedIndividual;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.junit.Test;

/**
 * Class for testing purposes.
 */

public class OntologyPermuterAndUtilsTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(OntologyPermuterAndUtilsTest.class);

    public OntologyPermuterAndUtilsTest() { }
    
    @Test
    public void testOntologyPermute() throws OWLOntologyCreationException {
        OntologyPermuter op = new OntologyPermuter(true);
        op.loadOntology("src/test/resources/test.owl");
        op.permute();
    }
    

    //@Test // Missing file subset-example.owl
    public void testOntologySubsetOf() throws Exception {
        OntologyPermuter op = new OntologyPermuter("test-permutations");
        op.loadOntology("subset-example.owl");

        subsetTest(op.getOntology(), op.getFactory());
    }

    private void subsetTest(OWLOntology ontology, OWLDataFactory factory) {
        Set<OWLClass> classes = ontology.getClassesInSignature();
        OWLClass c1 = classes.iterator().next();

        Set<OWLNamedIndividual> individuals = ontology.getIndividualsInSignature();
        OWLNamedIndividual i1 = individuals.iterator().next();

        OWLClassAssertionAxiom ax1 = factory.getOWLClassAssertionAxiom(c1, i1);
        OWLClassAssertionAxiom ax2 = factory.getOWLClassAssertionAxiom(c1, i1);

        Set<OWLClassAssertionAxiom> set1 = new HashSet<>();
        set1.add(ax1);

        Set<OWLClassAssertionAxiom> set2 = new HashSet<>();
        set2.add(ax2);

        Set<Set<OWLClassAssertionAxiom>> mainSet = new HashSet<>();
        mainSet.add(set2);

        LOGGER.info("{}", Utils.subsetOf(set1, mainSet));
    }

    /**
     * Tests Util.subSetOf()
     */
    @Test
    public void testSubsetOf() {
        Set<String> set1 = new HashSet<>();
        set1.add("a");
        set1.add("b");
        set1.add("c");

        Set<Set<String>> setofsets = new HashSet<>();
        setofsets.add(set1);

        Set<String> set2 = new HashSet<>();
        set2.add("c");

        Set<String> set3 = new HashSet<>();
        set3.add("d");

        LOGGER.info("set2: {}", Utils.subsetOf(set2, setofsets));
        LOGGER.info("set3: {}", Utils.subsetOf(set3, setofsets));
    }

    /**
     * For each class, print if true if it's a leaf class otherwise false.
     * @throws OWLOntologyCreationException 
     *
     */
    //@Test // Missing file "subset-example.owl
    public void testIsLeafClass() throws OWLOntologyCreationException {
        OntologyPermuter op = new OntologyPermuter("test-permutations");
        op.loadOntology("subset-example.owl");
        Set<OWLClass> classes = op.getOntology().getClassesInSignature();

        LOGGER.info("Start leaf class test");
        for (OWLClass c : classes) {
            LOGGER.info("{} : {}", c, op.isLeafClass(c));
        }
        LOGGER.info("End leaf class test");
    }

    /**
     * For each leaf class in the ontology, print its top super class.
     * @throws Exception
     */
    /*
    public void testTopSuperClassOf() throws Exception {
        ArrayList<OWLClassExpression> leafSubClasses = allLeafSubClasses(thing);

        for (OWLClassExpression leaf : leafSubClasses) {
            LOGGER.info(leaf + " : " + topSuperClassOf(leaf.asOWLClass()));
        }
    }
     */

    /**
     * Test of the permuteList method in class Utils
     */
    @Test
    public void testUtilPermute() {
        List<String> a1 = new ArrayList<>();
        List<String> a2 = new ArrayList<>();
        List<String> a3 = new ArrayList<>();

        a1.add("a");
        a1.add("b");
        a2.add("c");
        a2.add("d");
        a2.add("e");
        a3.add("f");
        a3.add("g");
        a3.add("h");

        List<List<String>> listOfLists = new ArrayList<>();
        listOfLists.add(a1);
        listOfLists.add(a2);
        listOfLists.add(a3);

        LOGGER.info("a2: {}", Utils.permuteList(a2));
        LOGGER.info("listOfLists: {}", Utils.permuteLists(listOfLists));

        List<Integer> b1 = new ArrayList<>();
        List<Integer> b2 = new ArrayList<>();
        List<Integer> b3 = new ArrayList<>();

        b1.add(1);
        b1.add(2);
        b1.add(3);
        b1.add(4);
        b2.add(1);
        b2.add(2);
        b2.add(3);
        b2.add(4);
        b3.add(5);
        b3.add(6);

        List<List<Integer>> l = new ArrayList<>();
        l.add(b1);
        l.add(b2);
        l.add(b3);

        LOGGER.info("l: {}", Utils.permuteLists(l));
    }

    @Test
    public void testNonEmptySubsetsOf() {
        Set<String> test = new HashSet<>();
        test.add("A");
        test.add("B");
        test.add("C");
        test.add("D");
        //test.add("E");

        LOGGER.info("test: {}", Utils.nonEmptySubsetsOf(test));
        LOGGER.info("# in test: {}", Utils.nonEmptySubsetsOf(test).size());
    }
}

