mod OWL-CONVERTER is
    protecting CONFIGURATION .
    protecting NAT .
    protecting STRING .
    protecting DEFINITION-GEO .
    protecting SPATIAL .

    sorts LeafClass ObjectAttribute .
    subsort LeafClass < Msg .
    subsort ObjectAttribute < Nat .

    --- Fault
    sorts FaultType FaultCapacity .
    subsorts FaultType FaultCapacity < LeafClass .

    op ReverseFault : -> FaultType . 
    op NormalFault : -> FaultType . 
    op SealingFault : -> FaultCapacity . 
    op NonSealingFault : -> FaultCapacity . 

    --- GeoUnit
    sorts RockType Permeability Porosity Location Kerogen Hydrocarbon .
    subsorts RockType Permeability Porosity Location Kerogen Hydrocarbon < LeafClass .

    op Sandstone : -> RockType .
    op Shale : -> RockType .
    op Permeable : -> Permeability .
    op NonPermeable : -> Permeability .
    op Porous : -> Porosity .
    op NonPorous : -> Porosity .
    op FeederChannel : -> Location .
    op DistributaryChannel : -> Location .
    op InterChannel1 : -> Location .
    op InterChannel2 : -> Location .
    op Lobe : -> Location .
    op BasinPlain : -> Location .

    op NoKerogen : -> Kerogen .
    op NoHydrocarbons : -> Hydrocarbon .

    --- From Crystal (with subsorts of ObjectAttribute added)
    subsort Nat < Oid .
    op null : -> Oid .

    --- Fault definition
    sorts Ftype Ffilling .

    --- Fault types related to external stress
    op normalfault : -> Ftype [ctor] .
    op reversefault : -> Ftype [ctor] .

    --- Fault filling
    op sealing : -> Ffilling .
    op non-sealing : -> Ffilling .
    
    --- Geo Rocks definition
    sorts Rtype SubmarinefanCA PermeabilityType PorosityType .
    op sandstone : -> Rtype .
    op shale : -> Rtype .

    --- Submarine Fan
    op feederChannel : -> SubmarinefanCA .
    op distributaryChannel : -> SubmarinefanCA .
    op interChannel : -> SubmarinefanCA .
    op lobe : -> SubmarinefanCA .
    op lobeFringe : -> SubmarinefanCA .
    op basinPlain : -> SubmarinefanCA .
    
    --- Permeability
    op permeable : -> PermeabilityType . 
    op non-permeable : -> PermeabilityType .
    
    --- Porosity
    op porous : -> PorosityType . 
    op non-porous : -> PorosityType .

    subsorts Ftype Ffilling Rtype SubmarinefanCA PermeabilityType PorosityType < ObjectAttribute .
    --- End From Crystal

    --- Fault object
    op <_: Fault | FType:_, SealingCapacity:_ > :
	      Oid Ftype Ffilling -> Object [ctor] .

    --- GeoUnit object
    ---op <_: GeoUnit | GeoUnit:_, Type:_, Permeability:_, Porosity:_, SubmarineFan:_, KerogenType:_, Hydrocarbon:_> :
	      ---Oid Oid Rtype PermeabilityType PorosityType SubmarinefanCA Nat Oid -> Object [ctor] .

    op <_: GeoUnit | Permeability:_, Porosity:_, SubmarineFan:_> :
        Oid PermeabilityType PorosityType SubmarinefanCA -> Object [ctor] .

    --- variables
    vars C : Configuration .
    vars S S' : Configuration .
    vars M : Msg .
    vars O O' : Oid .
    vars FC : FaultCapacity .
    vars FT : FaultType .
    vars RT : RockType .
    vars PB : Permeability .
    vars PR : Porosity .
    vars L : Location .
    var KT : Kerogen .
    var HC : Hydrocarbon .

    --- convert maps OWL leaf classes to object/class propertes in Maude
    op convert : LeafClass -> ObjectAttribute .

    --- Fault SealingCapacity
    eq convert(SealingFault) = sealing .
    eq convert(NonSealingFault) = non-sealing .

    --- Fault Type
    eq convert(NormalFault) = normalfault .
    eq convert(ReverseFault) = reversefault .

    --- GeoUnit Type
    eq convert(Sandstone) = sandstone .
    eq convert(Shale) = shale .

    --- GeoUnit Permeability
    eq convert(Permeable) = permeable .
    eq convert(NonPermeable) = non-permeable .

    --- GeoUnit Porosity
    eq convert(Porous) = porous .
    eq convert(NonPorous) = non-porous .

    --- GeoUnit SubmarineFan
    eq convert(FeederChannel) = feederChannel .
    eq convert(DistributaryChannel) = distributaryChannel .
    eq convert(InterChannel1) = interChannel .
    eq convert(InterChannel2) = interChannel .
    eq convert(Lobe) = lobe .
    eq convert(BasinPlain) = basinPlain .

    --- Kerogen
    eq convert(NoKerogen) = 0 .

    --- Hydrocarbon
    eq convert(NoHydrocarbons) = null .

    --- operator to construct maude objects from an OWL message
    op type : LeafClass Oid -> Msg .

    --- construct Fault
    eq type(FT, O) type(FC, O)
      =
        < O : Fault | FType: convert(FT), SealingCapacity: convert(FC) > .

    ---eq type(RT, O) type(PB, O) type(PR, O) type(L, O) type(KT, O) type(HC, O)
      ---=
        ---< O : GeoUnit | GeoUnit: O, Type: convert(RT), Permeability: convert(PB),
                        ---Porosity: convert(PR), SubmarineFan: convert(L),
                        ---KerogenType: convert(KT), Hydrocarbon: convert(HC) > .
    
    eq type(PB, O) type(PR, O) type(L, O)
      =
        < O : GeoUnit | Permeability: convert(PB), Porosity: convert(PR), SubmarineFan: convert(L) > .
    
    --- to define all these will be very tedious... how to do it automatically?
    ---eq construct(GeoUnit(O) Shale(O) Permeable(O) Porous(O) FeederChannel(O) C)
      ---=
        ---< O : GeoUnit | GeoUnit: O, Type: shale, Permeability: permeable, Porosity: porous, SubmarineFan: feederChannel, KerogenType: "none", Hydrocarbon: null >
        ---construct(C) .

    --- Gathering and translating spatial predicates
    op PartOf : Oid Oid -> Msg .
    eq PartOf(O, O') = spatial(partOf(O, O')) .

    op NextTo : Oid Oid -> Msg .
    eq NextTo(O, O') = spatial(nextTo(O, O')) .

    op Touches : Oid Oid -> Msg .
    eq Touches(O, O') = spatial(touches(O, O')) .

    op DirectlyAbove : Oid Oid -> Msg .
    eq DirectlyAbove(O, O') = spatial(directlyAbove(O, O')) .

    --- Gather spatial all predicates 
    eq spatial(S) spatial(S') = spatial(S S') .

    --- constants
    op unknown1 : -> Configuration .
    eq unknown1 = type(SealingFault, 1) type(ReverseFault, 1) .
    
    op unknown2 : -> Configuration .
    eq unknown2 = type(NonSealingFault, 2) type(ReverseFault, 2) .

    op unknown3 : -> Configuration .
    eq unknown3 = type(SealingFault, 3) type(NormalFault, 3) .
    
    op unknown4 : -> Configuration .
    eq unknown4 = type(NonSealingFault, 4) type(NormalFault, 4) .

    op unknown5 : -> Configuration .
    eq unknown5 = type(Shale, 5) type(Permeable, 5) type(Porous, 5) type(FeederChannel, 5) type(NoKerogen, 5) type(NoHydrocarbons, 5) .

    op unknown6 : -> Configuration .
    eq unknown6 = type(Sandstone, 6) type(Permeable, 6) type(Porous, 6) type(FeederChannel, 6) type(NoKerogen, 6) type(NoHydrocarbons, 6) .

    op unknown7 : -> Configuration .
    eq unknown6 = type(Sandstone, 6) type(Permeable, 6) type(Porous, 6) type(FeederChannel, 6) type(NoKerogen, 6) type(NoHydrocarbons, 6)
        DirectlyAbove(1, 2) NextTo(2, 3) .

endm
