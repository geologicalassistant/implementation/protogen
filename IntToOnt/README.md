# IntToOnt

This is a program for translating exported interpretations from Petrel into an OWL-ontology
describing derived geological objects from the interpretations (faults, layers and geo-units).

Usage: IntToOnt <folder> [<out>]
where
  <folder> is the folder containing the interpretation files
  <out> is a filename used for writing the resulting Abox (optional: prints to Standard out if no file given)

## Input

The input is a folder of CSV-files where faults have a filename starting with the character 'F'
and horizons have a filename starting with the character 'H'.

More details on how these files are interpreted can be found in `FileParser.java`.

## Output

The output is an OWL-ontology containing individuals of type Fault, Layer and GeoUnit derived
from the input interpretations, together with the spatial relationships between these objects.

In the output ontology, individual faults have the same name as thier input files. Layers gets
a name equal to the top horizon's and the bottom horizon's filenames appended together separated
by an underscore (e.g. a layer having top horizon from file with name "H1" and bottom horizon
with name "H2" gets the name "H1_H2"). Similarly, a geo unit gets a name consisting of
the left-most fault's (if any) name, appended to its layer's name, appended to the right-most
fault's name (if any), all three separated by an underscore.

More details on what is included in the ontology can be found in `OntologyWriter.java` and the
vocaublary used can be found in `Vocabulary.java`.

## Known limitations

- The program currently only derives geo-units formed by faults splitting layers. Thus, it cannot
  derive geo-units that are formed from dome-like layers or other non-fault vertical barriers.
  However, this could be remedied by introducing another interpreted element (in adition to
  faults and horizons) such as "vertical barrier" that might be vertical/semi-vertical parts of horizons,
  and then introduce a corresponding extension of the `BasicGeoObject`-class, similar to how `Fault` and
  `Horizon` is currently implemented. Corresponding changes then also needs to be added to `GeoFactory`
  for creating such elements and to `OntologyWriter` for handling axioms related to such elements.
- The algorithms handles 3D objects, but only handles a single cross-section at the time. Thus, if
  files from multiple interpretations are included in the same folder, the program might produce
  erroneous results.
- The program only generates `nextTo`-relationships between a geoUnits and its right-most fault,
  and does _not_ generate `connectedThroughFault`-relationships between a geounit G1, a fault F, and
  a geounit G2 such that G1 is connected to G2 via F.
