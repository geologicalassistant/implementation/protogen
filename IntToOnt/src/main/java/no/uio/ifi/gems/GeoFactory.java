package no.uio.ifi.gems;

import java.util.Arrays;
import java.util.Collections;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Polygon;
import org.locationtech.jts.geom.PrecisionModel;

public enum GeoFactory {
    ;

    //private static final PrecisionModel geometryPrecision = new PrecisionModel(Math.pow(10, 10));
    private static final PrecisionModel geometryPrecision = new PrecisionModel(PrecisionModel.FIXED);
    private static final GeometryFactory FACTORY = new GeometryFactory(geometryPrecision);

    // epsilon used in buffering geometries to avoid non-noded intersections
    // private static final double epsilon = 1; //Math.pow(10, -1);

    public static BasicGeoObject makeBasicGeoObject(String id, Coordinate[] coords) {
        if (FileParser.isFault(id)) {
            return new Fault(id, coords);
        } else if (FileParser.isHorizon(id)) {
            return new Horizon(id, coords);
        } else {
            // TODO: Raise error
            return null;
        }
    }

    public static LineString createLineString(Coordinate[] coords) {
        return FACTORY.createLineString(coords);
    }

    public static Layer makeLayer(Horizon seg1, Horizon seg2) {
        return new Layer(makeId(seg1, seg2), seg1, seg2);
    }
    
    public static GeoUnit makeGeoUnit(Layer layer, Fault left, Fault right) {
        return new GeoUnit(makeId(layer, left, right), layer, left, right);
    }

    public static String makeId(Layer layer, Fault left, Fault right) {
        String l = left == null ? "" : left.getId() + "_";
        String r = right == null ? "" : "_" + right.getId();
        return l + layer.getId() + r;
    }
    
    public static String makeId(Horizon seg1, Horizon seg2) {
        // Return canonical id, i.e. top's id first, then bottom's
        return seg1.getHighestValue() < seg2.getHighestValue()
            ? seg1.getId() + "_" + seg2.getId()
            : seg2.getId() + "_" + seg1.getId();
    }
    
    public static Set<GeoUnit> makeGeoUnits(Set<Layer> layers, Set<Fault> faults) {
        List<Fault> faultLst = new LinkedList<>(faults);
        // Order faults from left to right (along X-axis)
        faultLst.sort((f1, f2) -> Double.compare(f1.getHighestX(), f2.getHighestX()));
        
        Set<GeoUnit> geoUnits = new HashSet<>();
        
        // Make geoUnits based on the layer, and the faults surrounding it
        // If no fault is on one of the sides, a null-pointer is used
        for (Layer layer : layers) {

            List<Fault> faultIter = new LinkedList<>(faultLst);
            Fault left = null; // Current left-fault
            Fault right = null; // Current right-fault

            while (!faultIter.isEmpty()) {
                left = right; // Old right becomes new left as we move right-wards

                while (!faultIter.isEmpty()) { // Skip non-intersecting faults
                    right = faultIter.remove(0); 

                    if (right.intersects(layer)) {
                        geoUnits.add(makeGeoUnit(layer, left, right));
                        break;
                    }
                }
            }
            // Add final geo-unit right of final fault (or entire layer if no faults)
            geoUnits.add(makeGeoUnit(layer, right, null));
        }
        return geoUnits;
    }

    public static Set<Layer> makeLayers(List<Horizon> segments) {

        Set<Layer> layers= new HashSet<>();
        List<Horizon> segs = new LinkedList<>(segments);
        // Order segments from bottom to top (along Y-axis)
        segs.sort((s1, s2) -> Double.compare(s1.getHighestValue(), s2.getHighestValue()));

        // Iterate from top to bottom, making Horizons from current top-most and closest segment
        // (Lower getHighestValue means higher up, as this value denotes depth)
        while (!segs.isEmpty()) {
            Horizon top = segs.remove(0);
            top.getClosestSegment(segs)
                .ifPresent(bottom -> layers.add(makeLayer(top, bottom)));
        }
        return layers;
    }

    // Code below commented out as it gives non-noded intersection exception, i.e. it is not possible
    // to intersect the Geometries returned by the methods with e.g. faults without fixes to precision,
    // however, these fixes might be dependent on the particular input geometries, and is
    // thus difficult to incorporate into a robust solution. However, we might need similar code
    // in the future in more complex cases.

    // public static List<Geometry> splitByFaults(Set<Horizon> horizons, Set<Fault> faults) {

    //     List<Geometry> toSplit = horizons.stream()
    //         .map(Horizon::getGeometry)
    //         .collect(Collectors.toList());
    //     List<Geometry> split = new LinkedList<>();

    //     for (Fault f : faults) {
    //         Geometry fg = f.getGeometry().buffer(epsilon);
    //         for (Geometry g : toSplit) {
    //             split.add(g.difference(fg));
    //         }
    //         toSplit = new LinkedList<>(split);
    //         split = new LinkedList<>();
    //     }
    //     return toSplit;
    // }

    // public static Layer makeHorizon(Horizon seg1, Horizon seg2) {

    //     List<Coordinate> coords = new LinkedList<>();
    //     Arrays.stream(seg1.getGeometry().getCoordinates()).forEach(coords::add);

    //     if (seg1.getEnd().distance(seg2.getStart()) < seg1.getStart().distance(seg2.getEnd())) {
    //         // Add seg2's coords in normal order
    //         Arrays.stream(seg2.getGeometry().getCoordinates()).forEach(coords::add);
    //     } else {
    //         // Add seg2's coords in reversed order
    //         Coordinate[] seg2Coords = seg2.getGeometry().getCoordinates();
    //         for (int i = seg2Coords.length - 1; i >= 0; i--) {
    //             coords.add(seg2Coords[i]);
    //         }
    //     }
    //     coords.add(seg1.getStart().getCoordinate());

    //     Polygon horizonGeom = FACTORY.createPolygon(coords.toArray(new Coordinate[coords.size()]));

    //     return new Layer(makeId(seg1, seg2), horizonGeom);
    // }
}
