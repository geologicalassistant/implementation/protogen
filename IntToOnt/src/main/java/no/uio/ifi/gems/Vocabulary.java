package no.uio.ifi.gems;

import org.semanticweb.owlapi.apibinding.OWLManager;
import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntologyManager;

public enum Vocabulary {
    ;

    public static final OWLOntologyManager ontManager = OWLManager.createOWLOntologyManager();
    public static final OWLDataFactory factory = ontManager.getOWLDataFactory();

    public static final String ONTOLOGY_NS = "http://gems.ifi.uio.no/ont/";
    public static final String DATA_NS = "http://gems.ifi.uio.no/scenario/";

    // Classes
    public static final OWLClass FAULT = getClass("Fault");
    public static final OWLClass LAYER = getClass("Layer");
    public static final OWLClass GEO_UNIT = getClass("GeoUnit");
    
    
    // ObjectProperties
    public static final OWLObjectProperty DIRECTLY_ABOVE = getObjectProperty("DirectlyAbove");
    public static final OWLObjectProperty NEXT_TO = getObjectProperty("NextTo");
    public static final OWLObjectProperty TOUCHES = getObjectProperty("Touches");
    public static final OWLObjectProperty PART_OF = getObjectProperty("PartOf");

    
    // Utils
    private static OWLClass getClass(String localName) {
        return factory.getOWLClass(IRI.create(ONTOLOGY_NS + localName));
    }

    private static OWLObjectProperty getObjectProperty(String localName) {
        return factory.getOWLObjectProperty(IRI.create(ONTOLOGY_NS + localName));
    }
}
