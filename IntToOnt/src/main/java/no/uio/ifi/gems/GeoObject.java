package no.uio.ifi.gems;

public interface GeoObject {
    
    String getId();

    /**
     * Checks whether this touches argument, where two objects
     * touch if their boundaries overlap but interiors are disjoint.
     * Here, a BasicGeoObjects boundary is itself and has no interior,
     * thus two BasicGeoObjects can never touch.
     * @param other
     *      Another Touchable to check if this touches.
     * @return
     *      True if this touches argument, false otherwise.
     */
    boolean touches(GeoObject other);

    /**
     * Checks whether this is part of argument, where one object A
     * is part of another B if A's boundary is strictly contained in B's boundary.
     * This is used for checking containment between GeoUnits and Layers.
     * Again, a BasicGeoObjects boundary is itself, thus two BasicGeoObjects can be
     * partOf-related.
     * @param other
     *      Another Touchable to check if has this as part.
     * @return
     *      True if this is part of argument, false otherwise.
     */
    boolean partOf(GeoObject other);
}
