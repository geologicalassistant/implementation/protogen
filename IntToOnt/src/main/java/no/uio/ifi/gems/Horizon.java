package no.uio.ifi.gems;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;

import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;
import org.locationtech.jts.geom.Point;

public class Horizon extends BasicGeoObject {

    private final Point start;
    private final Point end;
    private final double highestValue;

    public Horizon(String id, Coordinate[] coords) {
        super(id, GeoFactory.createLineString(coords));
        this.start = getGeometry().getStartPoint();
        this.end = getGeometry().getEndPoint();
        this.highestValue = findHighestValue(getGeometry().getCoordinates());
    }

    public double getHighestValue() {
        return this.highestValue;
    }

    public Point getStart() {
        return this.start;
    }

    public Point getEnd() {
        return this.end;
    }

    private static double findHighestValue(Coordinate[] coords) {
        return Arrays.stream(coords)
            .mapToDouble(Coordinate::getY)
            .max()
            .orElse(0); // TODO: Decide on value if empty
    }

    public double distance(Horizon other) {

        // Find the sum of the closest end points, used to match HorizonSegments of same Horizons
        double dist1 = this.start.distance(other.start) + this.end.distance(other.end);
        double dist2 = this.start.distance(other.end) + this.end.distance(other.start);
        return Math.min(dist1, dist2);
    }

    public Optional<Horizon> getClosestSegment(Collection<Horizon> segments) {

        return segments.stream()
            .filter(s -> !this.equals(s)) // Remove this from stream
            .min((s1, s2) -> Double.compare(distance(s1), distance(s2)));
    }

    @Override
    public String toString() {
        return "HorizonSegment(" + getId() + ", " + getGeometry().toString() + ")";
    }
}
