package no.uio.ifi.gems;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

public class ComplexGeoObject implements GeoObject {

    enum Role { TOP, BOTTOM, LEFT, RIGHT }

    private final Map<Role, BasicGeoObject> roles;
    private final String id;

    public ComplexGeoObject(String id, BasicGeoObject top, BasicGeoObject bottom,
        BasicGeoObject left, BasicGeoObject right) {

        this.id = id;
        this.roles = new HashMap<>();

        if (top != null) this.roles.put(Role.TOP, top);
        if (bottom != null) this.roles.put(Role.BOTTOM, bottom);
        if (left != null) this.roles.put(Role.LEFT, left);
        if (right != null) this.roles.put(Role.RIGHT, right);
    }

    public Optional<BasicGeoObject> getBasicObject(Role role) {
        return Optional.ofNullable(this.roles.get(role));
    }

    public Map<Role, BasicGeoObject> getRoleMap() {
        return this.roles;
    }
    
    @Override
    public String getId() {
        return id;
    }
    
    private Collection<BasicGeoObject> getComponents() {
       return getRoleMap().values()
           .stream()
           .filter(o -> o != null)
           .collect(Collectors.toList());
    }
    
    public boolean sameComponent(ComplexGeoObject other, Role thisComp, Role otherComp) {

        Optional<BasicGeoObject> thisCompVal = getBasicObject(thisComp);
        Optional<BasicGeoObject> otherCompVal = other.getBasicObject(otherComp);
        return thisCompVal.isPresent() && thisCompVal.equals(otherCompVal);
    }
    
    @Override
    public boolean partOf(GeoObject other) {
        if (other instanceof ComplexGeoObject) {
            // If this is a constrained part of other, then this is part of other
            return getComponents().containsAll(((ComplexGeoObject) other).getComponents());
        } else {
            return false;
        }
    }
    
    @Override
    public boolean touches(GeoObject other) {
        if (other instanceof BasicGeoObject) {
            return this.roles.values().stream().anyMatch(other::equals);
        } else if (other instanceof ComplexGeoObject) {
            return !Collections.disjoint(getComponents(), ((ComplexGeoObject) other).getComponents());
        } else {
            return false;
        }
    }
    
    @Override
    public boolean equals(Object other) {
        return other != null
            && other instanceof ComplexGeoObject
            && ((ComplexGeoObject) other).getId().equals(this.id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}