package no.uio.ifi.gems;


import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.locationtech.jts.geom.Coordinate;

public class FileParser {

    private static final String FAULT_START = "F";
    private static final String HORIZON_START = "H";
    private static final String MATCH_STR = "(" + FAULT_START + "|" + HORIZON_START + ")";

    private static final Function<String[], Double[]> faultCoordLineParser =
        l -> new Double[] { Double.valueOf(l[3]), Double.valueOf(l[4]), Double.valueOf(l[5]) };
    private static final Function<String[], Double[]> horizonCoordLineParser =
        l -> new Double[] { Double.valueOf(l[1]), Double.valueOf(l[2]), Double.valueOf(l[3]) };


    public static Stream<Path> getFiles(String folder) throws IOException {

        Path path = Path.of(folder);

        if (!Files.isDirectory(path)) {
            // TODO: Improve error-handling
            System.err.println("Path " + folder + " not a readable folder. Aborting.");
            System.exit(1);
        }

        return Files.list(path).filter(p -> startsWith(p, MATCH_STR));
    }

    public static Coordinate[] parseCoordinates(Path csv) {

        Function<String[], Double[]> parser = isFault(csv)
            ? faultCoordLineParser
            : horizonCoordLineParser;
        return parseCoordinates(csv, parser);
    }

    private static Coordinate[] parseCoordinates(Path csv, Function<String[], Double[]> parser) {
        try {
            List<Coordinate> lst = Files.readAllLines(csv)
                .stream()
                .map(l -> l.split("\\s+"))
                .map(parser)
                .map(FileParser::toCoordinate)
                .collect(Collectors.toList());
            return lst.toArray(new Coordinate[lst.size()]);
        } catch (IOException ex) {
            // TODO: Improve error-handling
            System.err.println(ex.getMessage());
            System.exit(1);
        }
        return null;
     }

    private static Coordinate toCoordinate(Double[] ds) {
        // Make the depth the Y-ordinate, as this would be height in a 2D cross-section
        return new Coordinate(ds[0], ds[2], ds[1]);
    }

    public static String toLocalName(Path path) {
        return path.toFile().getName();
    }

    private static boolean startsWith(Path path, String match) {
        return startsWith(toLocalName(path), match);
    }

    private static boolean startsWith(String localName, String match) {
        return localName.matches("^" + match + ".*");
    }

    public static boolean isFault(Path id) {
        return startsWith(id, FAULT_START);
    }

    public static boolean isFault(String id) {
        return startsWith(id, FAULT_START);
    }

    public static boolean isHorizon(Path id) {
        return startsWith(id, HORIZON_START);

    }
    public static boolean isHorizon(String id) {
        return startsWith(id, HORIZON_START);
    }
}
