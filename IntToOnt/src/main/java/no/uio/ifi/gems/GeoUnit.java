package no.uio.ifi.gems;

public class GeoUnit extends ComplexGeoObject {
    
    private final Layer layer;

    public GeoUnit(String id, Layer layer, Fault left, Fault right) {
        super(id,
            layer.getBasicObject(ComplexGeoObject.Role.TOP).get(),
            layer.getBasicObject(ComplexGeoObject.Role.BOTTOM).get(),
            left, right);
        this.layer = layer;
    }
    
    public Layer getLayer() {
        return this.layer;
    }

    @Override
    public String toString() {
        return "GeoUnit(" + getId() + ", " + getRoleMap().toString() + ")";
    }

    // This is directly above other if this' bottom horizon equals other's top horizon
    // and they share at least one fault
    public boolean isDirectlyAbove(GeoUnit other) {
        return sameComponent(other, Role.BOTTOM, Role.TOP)
                && (sameComponent(other, Role.LEFT, Role.LEFT)
                    || sameComponent(other, Role.RIGHT, Role.RIGHT));
    }
}
