package no.uio.ifi.gems;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class IntToOnt {

    private static Set<Fault> faults = new HashSet<>();
    private static Set<Horizon> horizons = new HashSet<>();
    private static Set<Layer> layers = new HashSet<>();
    private static Set<GeoUnit> geoUnits = new HashSet<>();

    public static void main(String[] args ) {

        if (args.length == 0) {
            printUsage();
            return;
        }

        execute(args);
    }

    private static void execute(String[] args) {

        try {

            FileParser.getFiles(args[0])
                .map(IntToOnt::makeGeoObject)
                .forEach(IntToOnt::populateFaultsAndSegments);

            layers.addAll(GeoFactory.makeLayers(new LinkedList<>(horizons)));
            geoUnits.addAll(GeoFactory.makeGeoUnits(layers, faults));

            String out = args.length >= 2 ? args[1] : null;
            new OntologyWriter(out).write(faults, layers, geoUnits);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private static BasicGeoObject makeGeoObject(Path path) {
        return GeoFactory.makeBasicGeoObject(FileParser.toLocalName(path),
                                        FileParser.parseCoordinates(path));
    }

    private static void populateFaultsAndSegments(BasicGeoObject go) {
        if (go instanceof Fault) {
            faults.add((Fault) go);
        } else {
            horizons.add((Horizon) go);
        }
    }

    private static void printUsage() {
        System.out.println("Usage: IntToOnt <folder> [<out>]\nwhere\n  "
            + "<folder> is the folder containing the interpretation files\n  "
            + "<out> is a filename used for writing the resulting Abox "
            + "(optional: prints to Standard out if no file given)");
    }
}
