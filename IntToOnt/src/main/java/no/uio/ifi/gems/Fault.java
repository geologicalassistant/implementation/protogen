package no.uio.ifi.gems;

import java.util.Arrays;

import org.locationtech.jts.operation.distance3d.Distance3DOp;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.LineString;

public class Fault extends BasicGeoObject {
    
    private static final double EPSILON = 0.1;
    private final double highestX;

    public Fault(String id, Coordinate[] coords) {
        super(id, GeoFactory.createLineString(coords));
        this.highestX = findHighestX(coords);
    }
    
    private double findHighestX(Coordinate[] coords) {
        return Arrays.stream(coords)
            .mapToDouble(Coordinate::getX)
            .max()
            .orElse(0); // TODO: Decide on value if empty
    }
    
    public boolean intersects(Layer layer) {
        LineString bot = layer.getBasicObject(ComplexGeoObject.Role.BOTTOM).get().getGeometry();
        return Distance3DOp.isWithinDistance(getGeometry(),bot, EPSILON);
    }
    
    public double getHighestX() {
        return this.highestX;
    }

    @Override
    public String toString() {
        return "Fault(" + getId() + ", " + getGeometry().toString() + ")";
    }
}
