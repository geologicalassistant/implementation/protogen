package no.uio.ifi.gems;

import org.locationtech.jts.geom.LineString;

public abstract class BasicGeoObject implements GeoObject {
    
    private final LineString geom;
    private final String id;

    public BasicGeoObject(String id, LineString geom) {
        this.id = id;
        this.geom = geom;
    }

    public LineString getGeometry() {
        return this.geom;
    }

    @Override
    public String getId() {
        return id;
    }
    
    @Override
    public boolean touches(GeoObject other) {
        if (other instanceof ComplexGeoObject) {
            return other.touches(this);
        } else {
            // No two BasicGeoObjects can touch
            return false;
        }
    }
    
    @Override
    public boolean partOf(GeoObject other) {
        return false;
    }
    
    @Override
    public boolean equals(Object other) {
        return other != null
            && other instanceof BasicGeoObject
            && ((BasicGeoObject) other).getId().equals(this.id);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}
