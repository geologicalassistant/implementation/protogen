package no.uio.ifi.gems;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Set;
import java.util.stream.Stream;

import org.semanticweb.owlapi.model.IRI;
import org.semanticweb.owlapi.model.OWLClass;
import org.semanticweb.owlapi.model.OWLDataFactory;
import org.semanticweb.owlapi.model.OWLIndividual;
import org.semanticweb.owlapi.model.OWLObjectProperty;
import org.semanticweb.owlapi.model.OWLOntology;
import org.semanticweb.owlapi.model.OWLOntologyCreationException;
import org.semanticweb.owlapi.model.OWLOntologyManager;
import org.semanticweb.owlapi.model.OWLOntologyStorageException;

import no.uio.ifi.gems.ComplexGeoObject.Role;

public class OntologyWriter {
    
    private static final String BASE_ONTOLOGY = "BaseOntology.owl";

    private final OWLOntologyManager ontManager;
    private final OWLDataFactory factory;
    private final String out;

    public OntologyWriter(String out) {
        this.ontManager = Vocabulary.ontManager;
        this.factory = Vocabulary.factory;
        this.out = out;
    }

    public void write(Set<Fault> faults, Set<Layer> horizons, Set<GeoUnit> geoUnits) {
        try {
            OWLOntology ont = makeOntology(faults, horizons, geoUnits);
            this.ontManager.saveOntology(ont, makeOutputStream(out));
        } catch (OWLOntologyStorageException|FileNotFoundException|OWLOntologyCreationException ex) {
            System.err.println(ex.getMessage());
            System.exit(1);
        }
    }

    private OWLOntology makeOntology(Set<Fault> faults, Set<Layer> layers, Set<GeoUnit> geoUnits)
        throws OWLOntologyCreationException {

        // Use the BaseOntology.owl as a basis for the output ontology
        OWLOntology ont = this.ontManager.loadOntologyFromOntologyDocument(getClass().getClassLoader().getResourceAsStream(BASE_ONTOLOGY));

        // Add faults
        addClassAssertion(faults.stream(), Vocabulary.FAULT, ont);

        // Add horizons
        addClassAssertion(layers.stream(), Vocabulary.LAYER, ont);

        // Add geo units
        addClassAssertion(geoUnits.stream(), Vocabulary.GEO_UNIT, ont);
        
        // Add relationships
        for (GeoUnit gu1 : geoUnits) {
            // Add partOf its layer
            addRelationship(gu1, Vocabulary.PART_OF, gu1.getLayer(), ont);

            // Add before
            gu1.getBasicObject(Role.RIGHT)
                .ifPresent(fault -> addRelationship(gu1, Vocabulary.NEXT_TO, fault, ont));

            // Add above
            for (GeoUnit gu2 : geoUnits) {
                if (gu1.isDirectlyAbove(gu2)) {
                    addRelationship(gu1, Vocabulary.DIRECTLY_ABOVE, gu2, ont);
                }
            }
        }
        return ont;
    }

    private void addRelationship(GeoObject subject, OWLObjectProperty pred, GeoObject object, OWLOntology ont) {
        OWLIndividual subInd = makeIndividual(subject);
        OWLIndividual objInd = makeIndividual(object);
        this.ontManager.addAxiom(ont, this.factory.getOWLObjectPropertyAssertionAxiom(pred, subInd, objInd));
    }
    
    private OWLIndividual makeIndividual(GeoObject obj) {
        return this.factory.getOWLNamedIndividual(IRI.create(Vocabulary.DATA_NS + obj.getId()));
    }

    private void addClassAssertion(Stream<? extends GeoObject> objs, OWLClass c, OWLOntology ont) {
        objs.map(GeoObject::getId)
            .map(id -> Vocabulary.DATA_NS + id)
            .map(IRI::create)
            .map(this.factory::getOWLNamedIndividual)
            .map(i -> this.factory.getOWLClassAssertionAxiom(c, i))
            .forEach(ax -> this.ontManager.addAxiom(ont, ax));
    }

    private OutputStream makeOutputStream(String path) throws FileNotFoundException {
        return path == null
            ? System.out
            : new FileOutputStream(path);
    }
}
