package no.uio.ifi.gems;

public class Layer extends ComplexGeoObject {

    public Layer(String id, Horizon top, Horizon bottom) {
        super(id, top, bottom, null, null);
    }

    @Override
    public String toString() {
        return "Horizon(" + getId() + ", " + getRoleMap().toString() + ")";
    }
}
