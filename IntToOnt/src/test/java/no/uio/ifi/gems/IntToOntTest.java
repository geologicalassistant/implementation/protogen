package no.uio.ifi.gems;

import java.nio.file.Paths; 

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class IntToOntTest {
    
    private static final String TOY_TEST_PATH =
            Paths.get("src", "test", "resources", "toy-interpretation").toString();

    @Test
    public void toyTest() {
        IntToOnt.main(new String[] { TOY_TEST_PATH });
    }
}
